- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    CGRect frame = CGRectZero;

    NSDate *date = [[self dataSource] getDateForEventByIndexPath:indexPath];

    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS
                                                       fromDate:date];

    NSInteger numberOfDayInGrid = _weekDaysShift + [components day] - 1;

    NSInteger x = numberOfDayInGrid % kWeekDaysCount;
    NSInteger y = numberOfDayInGrid / kWeekDaysCount;

    if (_eventsOffset[x][y] >= kEventsInDayMaxCount)
    {
        attributes.hidden = YES;
    }

    frame.origin = CGPointMake
            (
                    x * kDayWidth
                            + kEventLeftPadding,
                    y * kDayHeight
                            + kDayLabelWidth
                            + kDayLabelPadding * 2
                            + _eventsOffset[x][y] * (kEventHeight + kEventTopPadding)
            );

    frame.size = CGSizeMake
            (
                    kDayWidth - kEventLeftPadding - kEventRightPadding,
                    kEventHeight
            );

    frame.origin.y = (int) frame.origin.y;
    frame.origin.x = (int) frame.origin.x;

    attributes.frame = frame;
    attributes.zIndex = indexPath.row;
    attributes.alpha = kEventAlpha;

    _eventsOffset[x][y]++;

    return attributes;
}




////////////////






- (void)saveMOCInBackgroundInNewContextWithBlock:(void (^)(NSManagedObjectContext *))saveBlock
                                      completion:(void (^)())completion
{
    dispatch_block_t saveContextsBlock = ^
    {
        UIBackgroundTaskIdentifier identifier = [SharedApplication beginBackgroundTaskWithExpirationHandler:^{}];

        NSManagedObjectContext *localContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        localContext.parentContext = self.mainManagedObjectContext;

        [localContext performBlock:^
        {
            saveBlock(localContext);

            NSError *localContextError = nil;
            BOOL localContextSuccess = [localContext save:&localContextError];

            if (!localContextSuccess)
            {
                NSLog(@"Saving in context failed. %@", localContextError.userInfo);
            }

            [self.mainManagedObjectContext performBlock:^
            {
                NSError *mainContextError = nil;
                BOOL mainContextSuccess = [self.mainManagedObjectContext save:&mainContextError];

                if (!mainContextSuccess)
                {
                    NSLog(@"Saving in context failed. %@", mainContextError.userInfo);
                }

                if (completion)
                {
                    completion();
                }
            }];
        }];
        
        [SharedApplication endBackgroundTask:identifier];
    };
    
    if ([NSThread isMainThread])
    {
        saveContextsBlock();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), saveContextsBlock);
    }
}







////////////////







/* Parse and store all folders from incoming data */
- (void)handleFolders:(NSDictionary *)data
              context:(NSManagedObjectContext *)context
    dataAccessManager:(CDataAccessManager *)dataAccessManager
{
    NSArray *foldersElement = [data objectForKey:@"folders"];

    int i = 0;

    for (NSDictionary *f in foldersElement)
    {
        if ([f isKindOfClass:[NSNull class]])
        {
            continue;
        }

        [self handleFolder:f
                   context:context];

        if ((i != 0)
            && ((i % 50) == 0))
        {
            [dataAccessManager mediatorSave:context];
        }

        i++;
    }
}








/////////////




- (void)test_is_repetition_in_date_longer_task_2 {

    task = [CDCommonTask createAgileTaskWithBeginDate:[NSDate dateFromYear:2013 month:1 day:4] andEndDate:[NSDate dateFromYear:2013 month:1 day:5]];
    repetition.cdTask = (CDCommonTask *) task;

    repetition.mode = ARRepetitionModeWeekly;

    repetition.cdDaysInWeek = @[@NO, @YES, @NO, @NO, @YES, @NO, @YES];

    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:7]], @"");
    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:8]], @"");
    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:9]], @"");
    STAssertFalse([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:10]], @"");
    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:11]], @"");
    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:12]], @"");
    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:13]], @"");
    STAssertTrue([repetition isRepetitionInDate:[NSDate dateFromYear:2013 month:1 day:14]], @"");
}



- (BOOL)isRepetitionInDate:(NSDate *)date {

    if ([self isRepetitionBeginInDate:date]) {

        return YES;

    } else {

        int duration = [self.cdTask daysDuration];

        while (duration > 1) {

            date = [date dateDaysBefore:1];

            if ([self isRepetitionBeginInDate:date]) {

                return YES;
            }

            duration--;
        }
    }

    return NO;
}


